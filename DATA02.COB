       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA2.
       AUTHOR. Natthachai.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 AL-NUMERIC   PIC   X(5)    VALUE "A1234".
       01 NUM-INT      PIC   9(5).    
       01 NUM-NON-INT  PIC   9(3)V9(2).
       01 ALPHA        PIC   A(5).

       PROCEDURE DIVISION.
       BEGIN.
           MOVE AL-NUMERIC TO NUM-INT.
           DISPLAY "NUM-INT : " NUM-INT.

           MOVE AL-NUMERIC TO NUM-NON-INT.
           DISPLAY "NUM-NON-INT : " NUM-NON-INT.

           MOVE AL-NUMERIC TO ALPHA.
           DISPLAY "ALPHA : " ALPHA.
           GOBACK.
