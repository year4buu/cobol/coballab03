       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA3.
       AUTHOR. Natthachai.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 SURNAME           PIC    X(8)   VALUE "Jansri".
       01 SALE-PRICE        PIC   9(4)V99.
       01 NUM-OF-EMPLOYEES  PIC   999V99.
       01 SALARY            PIC   9999V99.
       01 COUNTRY-NAME      PIC   X(9).
       
       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "1 " SURNAME 
           MOVE "SUMID" TO SURNAME 
           DISPLAY "2 " SURNAME 
           MOVE "WILLIAM" TO SURNAME 
           DISPLAY "3 " SURNAME 

           .
           DISPLAY "1 " SALE-PRICE 
           MOVE ZERO TO SALE-PRICE  
           DISPLAY "2 " SALE-PRICE 
           MOVE 0025.500 TO SALE-PRICE  
           DISPLAY "3 " SALE-PRICE
      *    01 SALE-PRICE  PIC   9(4)V999.      0007.55
           MOVE 7.553 TO SALE-PRICE 
           DISPLAY "4 " SALE-PRICE 
      *    01 SALE-PRICE  PIC   9(4)V999.      3425.15
           MOVE 93425.158 TO SALE-PRICE  
           DISPLAY "5 " SALE-PRICE 
      *    01 SALE-PRICE  PIC   9(4)V999.      0128.00
           MOVE 128 TO SALE-PRICE  
           DISPLAY "6 " SALE-PRICE

      *    01 NUM-OF-EMPLOYEES  PIC   999V99.     012.40
           DISPLAY NUM-OF-EMPLOYEES  
           MOVE 12.4 TO NUM-OF-EMPLOYEES.
           DISPLAY NUM-OF-EMPLOYEES  
      *    01 NUM-OF-EMPLOYEES  PIC   999V99.     745.00
           DISPLAY NUM-OF-EMPLOYEES  
           MOVE 6745 TO NUM-OF-EMPLOYEES.
           DISPLAY NUM-OF-EMPLOYEES  

      *    01 SALARY            PIC   9999V99.    0745.00
           MOVE NUM-OF-EMPLOYEES TO SALARY 
           DISPLAY SALARY.

           MOVE "GALAWAY" TO COUNTRY-NAME 
           DISPLAY COUNTRY-NAME 
           MOVE ALL "@" TO COUNTRY-NAME 
           DISPLAY COUNTRY-NAME 
           MOVE ALL "@-$" TO COUNTRY-NAME 
           DISPLAY COUNTRY-NAME 
           GOBACK.  
