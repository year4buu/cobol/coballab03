       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA4.
       AUTHOR. Natthachai.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  STUDENT-RECORD-DATA  PIC X(44) VALUE "1205621WILLIAM  FITZPATRIC
      -    "K 19751021LM051385".
       01 STUDENT-RECORD.
          05 STUDENT-ID           PIC   9(7).
          05 STUDENT-NAME.  
             10 FORENAME          PIC X(9).
             10 SURNAME.
                15 F-SURNAME  PIC   X(1).
                15 FILLER         PIC   X(11).
          05 DATE-OF-BIRTH.
             08 YOB               PIC 9(4).
             08 MOB-DOB.
                10 MOB            PIC 9(2).
                10 DOB            PIC 9(2).
          05 COURSE-ID            PIC   X(5).
          05 GPA                  PIC   9V99.      
       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY STUDENT-RECORD-DATA.
           MOVE STUDENT-RECORD-DATA TO STUDENT-RECORD.
           DISPLAY STUDENT-RECORD.
           DISPLAY STUDENT-ID.
           DISPLAY STUDENT-NAME.
           DISPLAY FORENAME.
           DISPLAY SURNAME.
           DISPLAY F-SURNAME "." FORENAME.
           DISPLAY DATE-OF-BIRTH.
           DISPLAY YOB.
           DISPLAY MOB.
           DISPLAY DOB.
           DISPLAY DOB "/" MOB "/" YOB.
           DISPLAY MOB-DOB            
           DISPLAY COURSE-ID.
           DISPLAY GPA.           

           GOBACK.  
